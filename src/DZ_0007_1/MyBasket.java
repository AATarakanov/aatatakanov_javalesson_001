package DZ_0007_1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyBasket implements Basket
{

    private Map<String, Integer> list = new HashMap<>();

    @Override
    public void addProduct(String product, int quantity) {

        Integer q = list.get(product);

        if  (q == null)
        {
            list.put(product, quantity);
        }
        else
        {
            list.put(product, quantity + q);
        }

    }

    @Override
    public void removeProduct(String product) {
        list.remove(product);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {

        Integer q = list.get(product);

        if  (q == null)
        {
            list.put(product, quantity);
        }
        else
        {
            list.put(product, quantity);
        }

    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public List<String> getProducts() {
        List <String> l = new ArrayList<>(this.list.keySet());
        return l;
    }

    @Override
    public int getProductQuantity(String product) {
        return list.get(product);
    }

    public void prn()
    {
        System.out.println(list);
    }
}
