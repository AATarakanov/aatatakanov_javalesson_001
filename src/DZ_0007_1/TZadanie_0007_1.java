package DZ_0007_1;

import java.util.ArrayList;
import java.util.List;

public class TZadanie_0007_1
{
    public static void main(String[] args)
    {

        MyBasket MB = new MyBasket();

        MB.addProduct("P1", 1);
        MB.addProduct("P1", 2);
        MB.addProduct("P2", 3);
        MB.addProduct("P3", 4);
        MB.addProduct("P4", 5);
        MB.addProduct("P5", 6);

        MB.removeProduct("P3");

        MB.updateProductQuantity("P5", 9);

        MB.prn();



        List<String> l = new ArrayList<>( MB.getProducts() );
        System.out.println(l);

    }

}
