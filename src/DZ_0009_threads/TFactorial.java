package DZ_0009_threads;

import java.util.concurrent.CountDownLatch;
        import java.util.concurrent.ExecutorService;
        import java.util.concurrent.Executors;

public class TFactorial
{
    int maxValue;
    boolean isMultiThread;
    int nThreadsForCount = 5;


    public TFactorial(int maxValue, boolean isMultiThread, int nThreadsForCount )
    {
        this.maxValue = maxValue;
        this.isMultiThread = isMultiThread;
        this.nThreadsForCount = nThreadsForCount;

        if (maxValue > 0)
        {
            calculate();    // не очень правильно помещать это сюда, но для домашки сойдёт.
        }
        else
        {
            System.out.println("Введено отрицательное значение");
        }
    }

    public void calculate()
    {
        if (isMultiThread)
        {
            calculateMultiThread();
        }
        else
        {
            calculateJustOneMainThread();
        }
    }

    public void calculateJustOneMainThread()
    {
        System.out.println("Однопоточный режим :");

        long result = 1;
        for (int i = 2; i <= maxValue; i++)
        {
            result *= i;
            System.out.println("Факториал числа " + i + "! = " + result);
        }
    }

    public void calculateMultiThread()
    {
        System.out.println("Многопоточный режим :");
        // для выполнения задачи выделим себе пулл из nThreadsForCount потоков
        ExecutorService threadPool = Executors.newFixedThreadPool(nThreadsForCount);

        // для каждого числа от 1 до maxValue будет вызван свой поток, вычисляющий факториал
        for (int i = 1; i <= maxValue; i++)
        {
            CountDownLatch latch = new CountDownLatch(1);
            Runnable worker = new TThreadFactorialCalc(latch, i);
            threadPool.execute(worker);
            try
            {
                latch.await();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        threadPool.shutdown();
    }

}
