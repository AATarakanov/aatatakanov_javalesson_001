package DZ_0009_threads;

/* Задача: посчитать факториалы n последовательных чисел
   однопоточно и многопоточно с использованием пула потоков */

public class Test
{
    public static void main(String[] args)
    {
        // ввод значения, до которого необходимо посчитать факториалы
        int maxValue = 10;

        // однопоточный режим
        new TFactorial(maxValue, false, 0);

        // многопоточный режим
        new TFactorial(maxValue, true, 5);
    }
}
