package DZ_0009_threads;

import java.util.concurrent.CountDownLatch;

public class TThreadFactorialCalc extends Thread
{
    private int maxValue;
    private CountDownLatch latch;

    public TThreadFactorialCalc(CountDownLatch latch, int maxValue)
    {
        this.latch = latch;
        this.maxValue = maxValue;
    }

    @Override
    public void run()
    {
        long result = 1;
        for (int i = 2; i <= maxValue; i++)
        {
            result *= i;
        }

        System.out.println("Факториал числа " + maxValue + "! = " + result);
        latch.countDown();
    }
}
