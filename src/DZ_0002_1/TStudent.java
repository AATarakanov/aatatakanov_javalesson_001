package DZ_0002_1;

import java.time.LocalDate;

public class TStudent extends TPerson
{

    private

        String  faculty = "";
        byte    course = 0;
        int     group = 0;
        boolean bStipend = false;
        float   Stipend = 0;

    protected

         String   getFaculty() {    return faculty;    }
         byte     getCourse()  {    return course;     }
         int      getGroup()   {    return group;      }
         boolean  isStipend()  {    return bStipend;   }
         float    getStipend() {    return Stipend;    }

         void     setFaculty  (String faculty)    {        this.faculty = faculty;    }
         void     setStipend  (boolean stipend)   {        this.bStipend = stipend;   }
         void     setCourse   (byte course)       {        this.course = course;      }
         void     setGroup    (int group)         {        this.group = group;        }
         void     setStipend  (float stipend)     {        this.Stipend = stipend;    }

//    public
//
//        TStudent()
//        {
//            this.faculty = "";
//            this.course = 0;
//            this.group = 0;
//            this.bStipend = false;
//            Stipend = 0;
//        }


//    public    TStudent(String faculty, byte course, int group, boolean bStipend, float stipend)
//        {
//            this.faculty = faculty;
//            this.course = course;
//            this.group = group;
//            this.bStipend = bStipend;
//            Stipend = stipend;
//        }


    public    TStudent(String faculty, byte course, int group, boolean bStipend, float stipend, long aId, String aName, String aSurname, String aPatr, LocalDate D)
        {
//            this.SetId(aId);
//            this.SetName(aName);
//            this.SetSurname(aSurname);
//            this.SetPatr(aPatr);
//            this.SetBirthDate(D);

            super(aId,  aName,  aSurname,  aPatr,  D);

            this.faculty = faculty;
            this.course = course;
            this.group = group;
            this.bStipend = bStipend;
            Stipend = stipend;
        }

    @Override
    protected void Print()
    {
        super.Print();

        System.out.println("Факультет "             + this.faculty      );
        System.out.println("Курс "                  + this.course       );
        System.out.println("Група "                 + this.group        );
        System.out.println("Стипендиат "            + this.bStipend     );
        System.out.println("Стипендия "             + this.Stipend      );
    }


}
