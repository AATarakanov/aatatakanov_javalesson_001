package DZ_0002_1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class TMain
{

    private static ArrayList<TPerson> arr = new ArrayList();

    public static void main(String[] args)
    {
        TPerson person1 = new TPerson();
        TPerson person2 = new TPerson(0,"Ivan", "Ivanovich", "Ivanov",  LocalDate.now() );

//        TTeacher teacher1 = new TTeacher("Матан", true, 1000);
//        TStudent student1 = new TStudent("Радиотехника", (byte)1, 512, true, 100);
//        teacher1.Print();
//        student1.Print();

//        TTeacher teacher2 = new TTeacher();
//        TStudent student2 = new TStudent();
//        teacher2.Print();
//        student2.Print();

        arr.add(new TTeacher("Линейка", true, 1001, 100, "Петров", "Петр", "петрович", LocalDate.now() ));
        arr.add(new TStudent("Радиотехника", (byte)1, 512, true, 100, 102 ,"Сидоров", "Сидор", "Сидорович", LocalDate.now() ));

//        for(int i=0; i<arr.size();i++){
//            //((TPerson)arr.get(i)).Print();
//            arr.get(i).prn();

            for (TPerson i:arr)
            {
                i.prn();
            }


    }


}
