package DZ_0002_1;

import java.time.LocalDate;

public class TTeacher extends TPerson
{

    private    String  subject = "";
    private    boolean bActive = false;
    private    float   salary = 0;

    protected    String   getSubject() {    return subject;    }
    protected    boolean  getSalary()  {    return bActive;    }
    protected    float    getStipend() {    return salary;     }
    protected    boolean  isbActive()  {    return bActive;    }

    protected    void setSubject     (String subject)    {        this.subject = subject;  }
    protected    void setSalary      (boolean salary)    {        this.bActive = salary;   }
    protected    void setbActive     (boolean bActive)   {        this.bActive = bActive;  }
    protected    void setSalary      (float salary)      {        this.salary = salary;    }

//    private TTeacher()
//        {
//            this.subject = "";
//            this.bActive = false;
//            this.salary = 0;
//        }

//    public    TTeacher(String subject, boolean bActive, float salary)
//        {
//            this.subject = subject;
//            this.bActive = bActive;
//            this.salary = salary;
//        }

    public TTeacher(String subject, boolean bActive, float salary, long aId, String aName, String aSurname, String aPatr, LocalDate D)
    {
        super( aId,  aName,  aSurname,  aPatr,  D);

        this.subject = subject;
        this.bActive = bActive;
        this.salary = salary;
    }

    @Override
    protected void Print()
    {
            super.Print();

            System.out.println("предмет "           + this.subject  );
            System.out.println("Преподает "         + this.bActive  );
            System.out.println("Зарплата "          + this.salary   );
    }

}
