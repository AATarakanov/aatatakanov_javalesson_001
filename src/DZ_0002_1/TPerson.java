package DZ_0002_1;

import java.time.LocalDate;

// объявление класса TPerson
public class TPerson
{
    // переменные класса TPerson
    private    long        id;         // ID
    private    String      name;       // Фамилия
    private    String      surname;    // имя
    private    String      patr;       // отчество
    private    LocalDate   birthDate;  // Дата рождения

    // геттеры
    protected            long        GetId()          { return  id;       }
    protected            String      GetName()        { return name;      }
    protected            String      GetSurname()     { return surname;   }
    protected            String      GetPatr()        { return patr;      }
    protected            LocalDate   GetBirthDate()   { return birthDate; }

    // сеттеры
    protected        void SetId(long lId)                { id = lId;}
    protected        void SetName(String sName)          { name = sName; }
    protected        void SetSurname(String sSurname)    { surname = sSurname; }
    protected        void SetPatr(String sPatr)          { patr = sPatr; }
    protected        void SetBirthDate (LocalDate D)     { this.birthDate = LocalDate.of(D.getYear(),D.getMonth(), D.getDayOfMonth());}

    protected       void SetTPerson(long aId, String aName, String aSurname, String aPatr, LocalDate D)
        {
            id      = aId;
            name = aName;
            surname = aSurname;
            patr = aPatr;
            this.birthDate = LocalDate.of(D.getYear(),D.getMonth(), D.getDayOfMonth());
        }

    // Конструктор по умолчанию
    public    TPerson()
        {
            this.id         = 0;
            this.name       = "";
            this.surname    = "";
            this.patr       = "";
            this.birthDate  = LocalDate.now();
        }

    // Конструктор обычный
    public TPerson(long aId, String aName, String aSurname, String aPatr, LocalDate D)
        {
            this.id      = aId;
            this.name = aName;
            this.surname = aSurname;
            this.patr = aPatr;
            this.birthDate = LocalDate.of(D.getYear(),D.getMonth(), D.getDayOfMonth());
        }

    // печать
    protected void Print()
        {
            System.out.println("ID "                + this.id  );
            System.out.println("Имя "               + this.name  );
            System.out.println("Фамилия "           + this.surname  );
            System.out.println("Отчество "          + this.patr       );
            System.out.println("Дата рождения  "    + this.birthDate.toString() );
        }

    // Эксперименты с ООП в Джаве
    public void prn()
    {
        System.out.println("===========================================================================");
        Print();
        System.out.println("===========================================================================");
    }
}
