package DZ_0007_2;

import java.util.HashMap;
import java.util.Map;

public class TUniqueChars {

    private String text;
    private Map hashMap = new HashMap<Character, Integer>();


    public TUniqueChars(String text) {
        this.text = text;
    }

    public String getText() {
//        return "TODO  - realize method calculate and change method getText()";
        //return text;
        return hashMap.toString();
    }

    public void setText(String text) {
        this.text = text;
    }

    public void calculate() {

        int StringLength = text.length();

        for (int i = 0; i < StringLength; i++) {
            Integer frequency = 1;
            char simvol = text.charAt(i);
            if (hashMap.containsKey(simvol))
            {
                frequency = (int) hashMap.get(simvol);
                frequency++;
            }
            hashMap.put(simvol, frequency);
        }

    }

    public void print_current_map() {

        System.out.println(hashMap);

    }

}
