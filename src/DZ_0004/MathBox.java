package DZ_0004;

import java.math.*;
import java.util.*;

public class MathBox
{

    // Пункт 1 ДЗ
    private Set<Number> numbers;

    public MathBox(Number... numbers)
    {
        this.numbers = new HashSet(Arrays.asList(numbers));
    }
    // Пункт 1 ДЗ

    public static void main(String[] args)
    {
        System.out.println(new MathBox(1, 2, 3));
        System.out.println(new MathBox(1, 2, 3).summator());
        System.out.println(new MathBox(new BigDecimal(5), new BigDecimal("100000000000000000000")).summator());
        System.out.println(new MathBox(1.1, 2.2, 3.7).summator());
        System.out.println(new MathBox(9.9, 6.6, 3.3).splitter(3.0));
        System.out.println(new MathBox(9.9, 6.6, 3.3).splitter(0.0));

    }

    // Пункт 4 ДЗ
    @Override
    public String toString()
    {
        return "MathBox{ numbers = " + numbers + '}';
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(numbers);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;

        if (!(obj instanceof MathBox)) return false;

        MathBox mathBox = (MathBox) obj;
        return Objects.equals(numbers, mathBox.numbers);
    }
    // Пункт 4 ДЗ



    // Пункт 2 ДЗ
    public Number summator()
    {
        if (numbers == null || numbers.isEmpty())
        {
            return null;
        }

        ArrayList<Number> arrayList = new ArrayList<>(numbers);

        Number sum = arrayList.get(0);
        if (sum == null) return null;

        for (int i = 1; i < arrayList.size(); i++)
        {
            if (sum instanceof BigDecimal) {
                sum = ((BigDecimal) sum).add((BigDecimal) arrayList.get(i));
            } else if (sum instanceof BigInteger) {
                sum = ((BigInteger) sum).add((BigInteger) arrayList.get(i));
            } else if (sum instanceof Byte) {
                sum = (Byte) sum + (Byte) arrayList.get(i);
            } else if (sum instanceof Double) {
                sum = (Double) sum + (Double) arrayList.get(i);
            } else if (sum instanceof Float) {
                sum = (Float) sum + (Float) arrayList.get(i);
            } else if (sum instanceof Integer) {
                sum = (Integer) sum + (Integer) arrayList.get(i);
            } else if (sum instanceof Long) {
                sum = (Long) sum + (Long) arrayList.get(i);
            } else if (sum instanceof Short) {
                sum = (Short) sum + (Short) arrayList.get(i);
            } else {
                throw new UnsupportedOperationException("Subclasses of Number only");
            }
        }
        return sum;
    }


    // Пункт 3 ДЗ
    public MathBox splitter(Number divider)
    {
        if (numbers == null || numbers.isEmpty())
        {
            throw new UnsupportedOperationException("cant operate with empty Set");
        }

        if (divider.floatValue() == 0)
        {
            throw new UnsupportedOperationException("Divizion by Zero");
        }

        Set<Number> result = new HashSet();
        for (Number number : numbers) {
            if (number instanceof BigDecimal) {
                result.add(((BigDecimal) number).divide((BigDecimal) divider));
            } else if (number instanceof BigInteger) {
                result.add(((BigInteger) number).divide((BigInteger) divider));
            } else if (number instanceof Byte) {
                result.add(((Byte) number / (Byte) divider));
            } else if (number instanceof Double) {
                result.add(((Double) number) / ((Double) divider));
            } else if (number instanceof Float) {
                result.add(((Float) number) / ((Float) divider));
            } else if (number instanceof Integer) {
                result.add(((Integer) number) / ((Integer) divider));
            } else if (number instanceof Long) {
                result.add(((Long) number) / ((Long) divider));
            } else if (number instanceof Short) {
                result.add(((Short) number) / ((Short) divider));
            } else {
                throw new UnsupportedOperationException("Not support any but subclasses of Number");
            }
        }
        numbers = result;
        return this;
    }


}
