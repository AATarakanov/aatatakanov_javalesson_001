package DZ_0008_proxy;

import java.lang.reflect.Proxy;

public class Test
{
    public static void main(String[] args)
    {
        TestTMathBox();
        TestProxy();
    }

    // TMathBox Test
    public static void TestTMathBox() {
        System.out.println("\nTest MathBox\n");
        TMathBox mathBox = new TMathBox(10, 20, 0, -10);
        System.out.println("  MathBox :" + mathBox);
        System.out.println("  Get x 3: " + mathBox.getMultiplied(3));
        System.out.println("  Min: " + mathBox.getMin());
        System.out.println("  Max: " + mathBox.getMax());
        mathBox.remove(0);
        System.out.println("  Remove zero: " + mathBox);
        System.out.println("  Sum: " + mathBox.getTotal());
        System.out.println("  Avg: " + mathBox.getAverage());
        System.out.println("  Map: " + mathBox.getMap());
    }

    // Proxy Test
    public static void TestProxy()
    {
        System.out.println("\nTest Proxy\n");
        IMathBoxable aMathBox = (IMathBoxable) Proxy.newProxyInstance(
                TMathBox.class.getClassLoader(),
                new Class[]{IMathBoxable.class},
                new TMathBoxProxy(new TMathBox(1, 2, 3, 4, 5, 6, 7, 8)));
        System.out.println(aMathBox);
        aMathBox.add(9);
        aMathBox.remove(9);
        System.out.println(aMathBox);
    }

}
