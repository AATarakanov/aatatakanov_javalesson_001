package DZ_0008_proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class TMathBoxProxy implements InvocationHandler {

    private TMathBox MathBoxProxied;

    public TMathBoxProxy(TMathBox AMathBoxProxied)
    {
        this.MathBoxProxied = AMathBoxProxied;
    }

    boolean isClearDataAnnotated(Method method) {
        return method.isAnnotationPresent(ClearData.class);
    }

    boolean isLogDataAnnotated(Method method) {
        return method.isAnnotationPresent(LogData.class);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
    {
        Object result;
        if (isLogDataAnnotated(method))
        {
            System.out.println("  IN PROXY elements before: " + MathBoxProxied.getElements());
        }

        result = method.invoke(MathBoxProxied, args);

        if (isClearDataAnnotated(method))
        {
            MathBoxProxied.getElements().clear();
            System.out.println("  IN PROXY elements cleared!");
        }
        if (isLogDataAnnotated(method))
        {
            System.out.println("  IN PROXY elements after: " + MathBoxProxied.getElements());
        }
        return result;
    }
}

