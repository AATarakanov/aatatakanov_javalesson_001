package DZ_0008_proxy;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class TMathBox implements IMathBoxable
{

    private Set<Integer> elements;

    public TMathBox() {
        this.elements = new HashSet<>();
    }

    public TMathBox(int... args)
    {
        this();
        for (int i : args)
        {
            elements.add(i);
        }
    }

    public Set<Integer> getElements() {
        return elements;
    }


    @Override
    public int getTotal() {
        int total = 0;
        for (Integer i : elements)
        {
            total += i;
        }
        return total;
    }

    @Override
    public int getAverage()
    {
        if (elements.isEmpty())
        {
            return 0;
        }
        else
        {
            return Math.round(getTotal() / elements.size());
        }
    }

    @Override
    public Set<Integer> getMultiplied(int multiplicator)
    {
        //коллекцию в поток, поэлементно умножаем, собираем из потока новую коллекцию
        Set<Integer> newCollection = elements.stream().map(integer -> integer * multiplicator).collect(Collectors.toSet());
        elements = newCollection; //заменяем старую коллекцию новой
        return elements;
        // Здесь уменя фрустрация, т.к. это классическая утечка
    }

    @Override
    public Map<Integer, String> getMap()
    {
        //коллекцию в поток, собираем из потока новую коллекцию
        return elements.stream().collect(Collectors.toMap(k -> k, v -> v.toString()));
    }

    @Override
    public int getMax()
    {
        return elements.stream().max(Integer::compare).get();
    }

    @Override
    public int getMin()
    {
        return elements.stream().min(Integer::compare).get();
    }

    @Override
    public boolean remove(int i) {
        return elements.remove(i);
    }

    @Override
    public boolean add(int i) {
        return elements.add(i);
    }

    @Override
    public String toString()
    {
        return "TMathBox: { elements: " + elements.toString() + " }";
    }

}
