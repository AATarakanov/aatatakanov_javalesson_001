package DZ_0002_2_vending.drinks;

public class HotDrink implements Drink
{
    private TDrinks TDrinks;
    private double price;

    public HotDrink(TDrinks TDrinks, double price)
    {
        this.TDrinks = TDrinks;
        this.price = price;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String getTitle() {
        return TDrinks.toString();
    }
}
