package DZ_0002_2_vending.drinks;

public interface Drink
{
    double getPrice();
    String getTitle();
}