package DZ_0002_2_vending;


import DZ_0002_2_vending.drinks.ColdDrink;
import DZ_0002_2_vending.drinks.Drink;
import DZ_0002_2_vending.drinks.HotDrink;
import DZ_0002_2_vending.drinks.TDrinks;


public class Main
{
    public static void main(String[] args)
    {
        Drink[] hotDrinks = new HotDrink[]{new HotDrink(TDrinks.tea, 30)};
        Drink[] coldDrinks = new ColdDrink[]{new ColdDrink(TDrinks.water, 70)};

        VendingMachine vm = new VendingMachine(hotDrinks);

        vm.addMoney(200);
        vm.giveMeADrink(0);

        vm.setDrinks(coldDrinks);
        vm.giveMeADrink(0);

    }
}