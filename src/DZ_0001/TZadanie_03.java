package DZ_0001;

import java.util.Scanner;

public class TZadanie_03
{
    public static void main(String[] args)
    {
        System.out.println("Введите размерность таблицы умножения не более 20");

        Scanner console = new Scanner(System.in);
        if (console.hasNextInt()) {
            int N = console.nextInt();
            if (N > 20)
            {
                N = 20; // не морочимся
            }

            for(int i = 1; i < N+1; i++)
            {
                for(int j = 1; j < N+1; j++)
                {
//                    System.out.print(j * i + " *");
                    System.out.printf("%4d", i*j);
                }
                System.out.println();
            }

        }
    }

}
