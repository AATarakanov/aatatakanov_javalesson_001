package DZ_0001;

import java.util.Scanner;

public class TZadanie_01
{
    public static void main(String[] args)
    {
        final  double gasPrice = 50.0;

        System.out.println("Введите количество литров:");

        Scanner console = new Scanner(System.in);
        if (console.hasNextInt()) {
            int N = console.nextInt();

            System.out.println("Цена:" + N*gasPrice);
        }
    }
}
