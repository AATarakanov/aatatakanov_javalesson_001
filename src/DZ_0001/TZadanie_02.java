package DZ_0001;

public class TZadanie_02
{
    public static void main(String[] args)
    {
        double[] myList = {1.9, 2.9, 3.4, 3.5};

        // Вывести на экран все элементы массива
        System.out.println("Сами числа:");
        for (int i = 0; i < myList.length; i++)
        {
            System.out.println(myList[i] + " ");
        }

         // Нахождение среди элементов массива наименьшего
        double min = myList[0];
        for (int i = 1; i < myList.length; i++)
        {
            if (myList[i] < min) min = myList[i];
        }
        System.out.println("Наименьший элемент: " + min);




    }
}
