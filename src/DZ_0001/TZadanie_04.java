package DZ_0001;

import java.util.Scanner;

public class TZadanie_04
{
    public static void main(String[] args) {
        System.out.println("ВВедите N для вычисления N! не более 20");

        Scanner console = new Scanner(System.in);
        if (console.hasNextInt()) {
            int N = console.nextInt();
            if (N > 20)
            {
                N = 20; // не морочимся
            }

            long factorial = 1L;
            for (int i = 1; i <= N; i++) {
                factorial = factorial * i;
            }

            //System.out.println("N!=" +     Long.toString( factorial ));
            //System.out.println("N!=" +     String.valueOf( factorial ));
            System.out.println("N!=" + factorial );
        }

    }

}
